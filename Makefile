PROG =	mle.X

SRCS =	main.f90 maximum_likelihood.f90 

OBJS =	main.o maximum_likelihood.o 

LIBS =	

FC = ifort
FFLAGS = -fast #-g -CB# -O
F90 = ifort
F90FLAGS = -fast #-g -CB # -O
LDFLAGS =  
all: $(PROG)

$(PROG): $(OBJS)
	$(F90) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

.PHONY: clean
clean:
	rm -f $(PROG) $(OBJS) *.mod

.SUFFIXES: $(SUFFIXES) .f .f90 .F90 .f95

.f90.o .f95.o .F90.o:
	$(F90) $(F90FLAGS) -c $<

.f.o:
	$(FC) $(FFLAGS) -c $<

main.o: maximum_likelihood.o
