MODULE maxlikelihood
IMPLICIT NONE
doubleprecision, parameter :: pi = 3.141592653589793239, tol = 1.d-8, &
                              maxtol=1.0d-4
    doubleprecision, allocatable       :: that_y(:)
    integer                             :: that_nn
contains

subroutine inverse(a1,c,n)
!============================================================
! Inverse matrix
! Method: Based on Doolittle LU factorization for Ax=b
! Alex G. December 2009
!-----------------------------------------------------------
! input ...
! a(n,n) - array of coefficients for matrix A
! n      - dimension
! output ...
! c(n,n) - inverse matrix of A
!===========================================================
doubleprecision, intent(out)    :: c(n,n)
integer, intent(in)             :: n
double precision, intent(in)    :: a1(n,n)
double precision                :: L(n,n), U(n,n), b(n), d(n), x(n), a(n,n), &
                                   coeff
integer                         :: i, j, k
a = a1
! step 0: initialization for matrices L and U and b
! Fortran 90/95 aloows such operations on matrices
L=0.0
U=0.0
b=0.0

! step 1: forward elimination
do k=1, n-1
   do i=k+1,n
      coeff=a(i,k)/a(k,k)
      L(i,k) = coeff
      do j=k+1,n
         a(i,j) = a(i,j)-coeff*a(k,j)
      end do
   end do
end do

! Step 2: prepare L and U matrices 
! L matrix is a matrix of the elimination coefficient
! + the diagonal elements are 1.0
do i=1,n
  L(i,i) = 1.0
end do
! U matrix is the upper triangular part of A
do j=1,n
  do i=1,j
    U(i,j) = a(i,j)
  end do
end do

! Step 3: compute columns of the inverse matrix C
do k=1,n
  b(k)=1.0
  d(1) = b(1)
! Step 3a: Solve Ld=b using the forward substitution
  do i=2,n
    d(i)=b(i)
    do j=1,i-1
      d(i) = d(i) - L(i,j)*d(j)
    end do
  end do
! Step 3b: Solve Ux=d using the back substitution
  x(n)=d(n)/U(n,n)
  do i = n-1,1,-1
    x(i) = d(i)
    do j=n,i+1,-1
      x(i)=x(i)-U(i,j)*x(j)
    end do
    x(i) = x(i)/u(i,i)
  end do
! Step 3c: fill the solutions x(n) into column k of C
  do i=1,n
    c(i,k) = x(i)
  end do
  b(k)=0.0
end do
end subroutine inverse

FUNCTION random_normal() RESULT (ran_norm)
    ! Adapted from the following Fortran 77 code
    !      ALGORITHM 712, COLLECTED ALGORITHMS FROM ACM.
    !      THIS WORK PUBLISHED IN TRANSACTIONS ON MATHEMATICAL SOFTWARE,
    !      VOL. 18, NO. 4, DECEMBER, 1992, PP. 434-435.

    !  The function random_normal() returns a normally distributed pseudo-random
    !  number with zero mean and unit variance.   This version uses the default
    !  uniform random number generator which is in your fortran library.

    !  The algorithm uses the ratio of uniforms method of A.J. Kinderman
    !  and J.F. Monahan augmented with quadratic bounding curves.

    !  Fortran 90 version by Alan Miller (alan @ mel.dms.csiro.au)

    IMPLICIT NONE
    REAL :: ran_norm

    !     Local variables
    REAL, PARAMETER :: s = 0.449871, t = -0.386595, a = 0.19600, b = 0.25472,  &
                       half = 0.5, r1 = 0.27597, r2 = 0.27846
    REAL            :: u, v, x, y, q

    !     Generate P = (u,v) uniform in rectangle enclosing acceptance region

    DO
      CALL RANDOM_NUMBER(u)
      CALL RANDOM_NUMBER(v)
      v = 1.7156 * (v - half)

    !     Evaluate the quadratic form
      x = u - s
      y = ABS(v) - t
      q = x**2 + y*(a*y - b*x)

    !     Accept P if inside inner ellipse
      IF (q < r1) EXIT
    !     Reject P if outside outer ellipse
      IF (q > r2) CYCLE
    !     Reject P if outside acceptance region
      IF (v**2 < -4.0*LOG(u)*u**2) EXIT
    END DO

    !     Return ratio of P's coordinates as the normal deviate
    ran_norm = v/u
    RETURN
END FUNCTION random_normal

SUBROUTINE gen_observations(n, y)
    integer, intent(in)                         :: n
    doubleprecision, allocatable, intent(out)   :: y(:)    
    INTEGER, ALLOCATABLE            :: seed(:)
    integer                         :: k, i, ISEED(4)
 ALLOCATE (y(n))
!     Set the random number seed.
CALL RANDOM_SEED(size=k)
ALLOCATE (seed(k))
CALL RANDOM_SEED(get=seed)

!Generate n random normals using TOMS 712
DO i = 1, n
  y(i) = random_normal()
END DO
END SUBROUTINE gen_observations

subroutine log_likelihood(y, theta, lli, ll)
    doubleprecision, allocatable, intent(in)    :: y(:) 
    doubleprecision, intent(in)                 :: theta(2)
    doubleprecision, allocatable, intent(out)   :: lli(:)
    doubleprecision, intent(out)                :: ll
    integer                                     :: i
ALLOCATE (lli(size(y)))
lli = 0.0d0
ll = 0.0d0
do i = 1, size(y)
    lli(i) = -log(sqrt(theta(2))) - 0.5*log(2.0d0*pi) &
           - (1.0d0/(2.0d0*theta(2)))*((y(i)-theta(1))**2)
end do
ll = sum(lli)
end subroutine log_likelihood

subroutine gradp(y, nn, nk, x0, s)
    doubleprecision, allocatable, intent(in)    :: y(:) 
    integer, intent(in)                 :: nn, nk
    doubleprecision, intent(in)         :: x0(nk)
    doubleprecision, intent(out)        :: s(nn,nk)
    doubleprecision                     :: sum_loglikelihood
    doubleprecision, allocatable        :: ff0(:)
    doubleprecision, dimension(nk)      :: xdh
    integer                             :: i, p
    doubleprecision, allocatable        :: lli(:)
    doubleprecision                     :: h, f0(nn)
!Initialize 
CALL log_likelihood(y, x0, ff0, sum_loglikelihood)
f0 = ff0
p=0
do i=1, nk
    p=p+1
    xdh = x0
    h = max(1.0D-6,  min(abs(x0(i)*1.0D-4), 1.0D+00))
    xdh(i) = x0(i) + h
    CALL log_likelihood(y, xdh, ff0, sum_loglikelihood)
    s(:,i) = (ff0-f0)/h
end do
end subroutine gradp

subroutine gradan(y, nn, nk, x0, s)
    doubleprecision, allocatable, intent(in)    :: y(:) 
    integer, intent(in)                 :: nn, nk
    doubleprecision, intent(in)         :: x0(nk)
    doubleprecision, intent(out)        :: s(nn,nk)
    integer                             :: i
do i=1,nn
    s(i,1) = (y(i)-x0(1))/x0(2)
    s(i,2) = -1.0/(2.0d0*x0(2))+(1.0d0/(2.0d0*x0(2)**2))*((y(i)-x0(1))**2)
end do
end subroutine gradan

subroutine MLE(param1, fmax, se)
    integer, parameter                                      :: nn=300000, nk=2
    doubleprecision, DIMENSION(:), allocatable              :: param0, ff0
    doubleprecision, DIMENSION(:), intent(out), allocatable ::param1, se
    doubleprecision, dimension(:,:), allocatable            :: s, h, h_inv, g, &
                                                               d, cov
    doubleprecision, intent(out)                            :: fmax
    integer                                                 :: ret
    integer                                                 :: it, i, j,info
    doubleprecision   :: step, sum_loglikelihood, f0, cm(1,1)
    doubleprecision, parameter                              :: maxhalf=10.0d0
    integer, parameter                                      :: maxiter=10000
    doubleprecision, allocatable       :: lli(:), y(:)
    doubleprecision :: THETA2(2)
ALLOCATE(param0(nk))
ALLOCATE(param1(nk))
ALLOCATE(se(nk))
ALLOCATE(ff0(nn))
ALLOCATE(g(nk,1))
ALLOCATE(s(nn,nk))
ALLOCATE(h(nk,nk))
ALLOCATE(h_inv(nk,nk))
ALLOCATE(d(nk,1))
ALLOCATE(cov(nk,nk))
!Initialize
param0(1) = 3.10d0
param0(2) = 2.1d0
CALL gen_observations(nn, y)

it = 0 !Iteration counter
step = 1.0d0
param1 = param0
CALL log_likelihood(y, param0, lli, sum_loglikelihood)
do i = 1, nn
    ff0(i) =  lli(i)
end do
f0 = sum_loglikelihood/DBLE(nn)
CALL gradp(y, nn, nk, param0, s)                                !NUMERICAL SCORE
!CALL gradan(y, nn, nk, param0, s)                             !Analitical SCORE
do i=1, nk
    g(i,1) = sum(s(:,i))/dble(nn)                                      !Gradient
end do
!Use outer product of the gradient as approximation to hessian
h = (1.0d0/dble(-nn))*matmul(transpose(s),s)        !Approximated Hessian Matrix
CALL inverse(-h, h_inv,nk)
cm = matmul(matmul(transpose(g),h_inv),g)    !Convergence measure = g'*inv(-h)*g
!if (proc_num==0) then
write (6,'(A80)'), '===========================================================&
                    ========================================='
write (6,'(A27,A26,A27)'), 'Param ', 'Param Value ', 'Gradient'
do i=1,nk
    write (6,'(i27, f26.4, f27.4)'), i, param0(i), g(i,1)
end do
write (6,'(A80)'), '----------------------------------------------------&
                            ------------------------------------------------'
!MAIN LOOP
do while(abs(cm(1,1)) .gt. maxtol)         !loop until convergence maxtol=1.0d-4
    param0 = param1                             !set initial value of parameters 
    CALL inverse(-h, h_inv,nk)
    d = step*matmul(transpose(g),h_inv)    !Direction from param0=step*inv(-h)*g
    CALL log_likelihood(y, param0 + d(:,1), lli, sum_loglikelihood)
    ff0 = lli
    fmax = sum_loglikelihood/dble(nn)             !Likelihood at param1=param0+d
    if (fmax .ge. f0) then                        !If step result in an increase
        it=it+1
        param1 = param0 + d(:,1)                         !Update parameter value
        f0 = fmax
                                                           !PRINT ITERATION INFO
!if (proc_num==0) then
       write (6,'(A22, I4)'), 'Iteration:', it
       write (6,'(A22, f20.4)'), 'Function value:', fmax
       write (6,'(A22, f20.4)'), 'Convergence measure:', cm
       write (6,'(A22, f20.4)'), 'Stepsize:', step
       write (6,'(A80)'), '====================================================&
                            ================================================'
       write (6,'(4A20)'), 'Param ', 'Param Value ', 'Direction', 'Gradient'
       write (6,'(A80)'), '----------------------------------------------------&
                            ------------------------------------------------'
       do i=1,nk
            write (6,'(i20, f20.4, f20.16, f20.4)'), i,param1(i), d(i,1), g(i,1)
       end do
!end if
       step = 1.0d0                                              !Reset stepsize
       CALL gradp(y, nn, nk, param1, s)                    !NUMERICAL SCORE
       !CALL gradan(y, nn, nk, param0, s)                  !Analitical SCORE
       h = (1.0d0/dble(-nn))*matmul(transpose(s),s) !Approximated Hessian Matrix
       do i=1, nk
            g(i,1) = sum(s(:,i))/dble(nn)                              !Gradient
       end do
       CALL inverse(-h, h_inv,nk)

       cm = matmul(matmul(transpose(g),h_inv),g) !Convergence measure = 
                                                                   !g'*inv(-h)*g
       else                                       !If step result in an increase
            step = step/2.0d0                                 !Half the stepsize
            !if (proc_num==0) then
                !write (6,'(A80)'), 'linesearching...'
           ! end if
            if (step .le. 2**-maxhalf) then
                ret = 1
            end if
    end if
    if (it .ge. maxiter) then
        ret = 2
        exit
    end if
    ret = 0
end do

!Calculate Standard Errors
CALL inverse(-h, h_inv,nk)
cov = (1.0d0/dble(nn))*h_inv
!Use the inverse of outer-product of the score as covariance estimator
do i=1,nk
    se(i) = sqrt(cov(i,i))
end do
!PRINT THE FINAL OUTPUT
!if (proc_num==0) then
if (ret==0) then
    write (6,'(A80)'), '****************************************************&
                            ************************************************'
    write (6,'(A80)'), 'CONVERGENCE ACHIEVED'
    write (6,'(A80)'), '****************************************************&
                            ************************************************'
    elseif( ret==1) then
        write (6,'(A80)'), '***************************************************&
                            *************************************************'
        write (6,'(A80)'), 'WARNING - NO CONVERGENCE'
        write (6,'(A80)'), 'Stepsize too small Still no increase in likelihood'
        write (6,'(A80)'), '***************************************************&
                            *************************************************'
    elseif(ret==2) then
        write (6,'(A80)'), '***************************************************&
                            *************************************************'
        write (6,'(A80)'), 'WARNING - NO CONVERGENCE'
        write (6,'(A80)'), 'Maximum iterations reached without convercence'
        write (6,'(A80)'), '***************************************************&
                            *************************************************'
end if
write (6,'(A22, I4)'), 'Iteration:', it
write (6,'(A22, f20.4)'), 'Function value:', fmax
write (6,'(A22, f20.4)'), 'Convergence measure:', cm
       write (6,'(A80)'), '====================================================&
                            ================================================'
       write (6,'(4A20)'), 'Param ', 'Param Value ', 'Gradient', 's.e.'
       do i=1,nk
            write (6,'(i20, f20.4, f20.4, f20.4)'), i,param1(i), g(i,1), se(i)
       end do
!end if
end subroutine MLE

END MODULE maxlikelihood
