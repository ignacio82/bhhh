program MLE_test
use maxlikelihood
implicit none
doubleprecision                                         :: MU, SIGMA, ll
doubleprecision, DIMENSION(:), allocatable              :: param1, se
doubleprecision                                         :: fmax

CALL MLE(param1, fmax, se)

end program
