# BHHH Algorithm (Serial)

BHHH is an optimization algorithm in numerical optimization similar to Gauss–Newton algorithm. It is an acronym of the four originators: Berndt, B. Hall, R. Hall, and Jerry Hausman.

This branch if for the serial code


##To get this code and run it just do the following:

```BASH
$ mkdir bitbucket
$ cd bitbucket
$ git clone git@bitbucket.org:ignacio82/bhhh.git
Cloning into 'bhhh'...
$ cd bhhh
$ make
$ ./mle.X
```

## To switch to the Parallel/MPI branch
```BASH
$ git checkout ignacio82/MPI
$ make
$ mpiexec -n 2 ./mle.X
```
